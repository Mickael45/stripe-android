package com.example.stripe_2.model

data class TripReportInfo(val locationId: String, val tripId: String, val readerId: String)