package com.example.stripe_2.model

/**
 * PaymentIntentCreationResponse data model from example backend
 */
data class PaymentIntentCreationResponse(val intent: String, val secret: String)