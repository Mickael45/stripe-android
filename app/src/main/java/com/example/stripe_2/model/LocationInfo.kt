package com.example.stripe_2.model

import com.google.gson.annotations.SerializedName

data class LocationInfo(
    @SerializedName("display_name") val displayName: String,
    @SerializedName("line1") val line1: String,
    @SerializedName("line2") val line2: String?,
    @SerializedName("city") val city: String,
    @SerializedName("postal_code") val postalCode: String,
    @SerializedName("state") val state: String?,
    @SerializedName("country") val country: String,
)
