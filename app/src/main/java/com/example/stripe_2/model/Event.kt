package com.example.stripe_2.model

data class Event(val message: String, val method: String)