package com.example.stripe_2.model


data class TripInfo(val date_dep: String, val cod_lign: String, val hor_dep: String, val message: String)
