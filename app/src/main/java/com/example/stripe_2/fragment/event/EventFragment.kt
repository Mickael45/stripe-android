package com.example.stripe_2.fragment.event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.stripe_2.NavigationListener
import com.example.stripe_2.R
import com.example.stripe_2.databinding.FragmentEventBinding
import com.example.stripe_2.fragment.TerminalFragment
import com.example.stripe_2.model.Event
import com.example.stripe_2.network.ApiClient
import com.example.stripe_2.viewmodel.EventViewModel
import com.stripe.stripeterminal.Terminal
import com.stripe.stripeterminal.external.callable.BluetoothReaderListener
import com.stripe.stripeterminal.external.callable.Callback
import com.stripe.stripeterminal.external.callable.PaymentIntentCallback
import com.stripe.stripeterminal.external.callable.PaymentMethodCallback
import com.stripe.stripeterminal.external.models.CardPresentParameters
import com.stripe.stripeterminal.external.models.CollectConfiguration
import com.stripe.stripeterminal.external.models.DiscoveryMethod
import com.stripe.stripeterminal.external.models.PaymentIntent
import com.stripe.stripeterminal.external.models.PaymentIntentParameters
import com.stripe.stripeterminal.external.models.PaymentMethod
import com.stripe.stripeterminal.external.models.PaymentMethodOptionsParameters
import com.stripe.stripeterminal.external.models.ReadReusableCardParameters
import com.stripe.stripeterminal.external.models.ReaderDisplayMessage
import com.stripe.stripeterminal.external.models.ReaderInputOptions
import com.stripe.stripeterminal.external.models.TerminalException
import retrofit2.Call
import retrofit2.Response
import java.lang.ref.WeakReference
import java.util.Locale

/**
 * The `EventFragment` displays events as they happen during a payment flow
 */
class EventFragment : Fragment(), BluetoothReaderListener {

    companion object {
        const val TAG = "com.example.stripe_2.fragment.event.EventFragment"

        private const val AMOUNT = "com.example.stripe_2.fragment.event.EventFragment.amount"
        private const val EMAIL = "com.example.stripe_2.fragment.event.EventFragment.email"
        private const val CURRENCY = "com.example.stripe_2.fragment.event.EventFragment.currency"
        private const val REQUEST_PAYMENT = "com.example.stripe_2.fragment.event.EventFragment.request_payment"
        private const val READ_REUSABLE_CARD = "com.example.stripe_2.fragment.event.EventFragment.read_reusable_card"
        private const val SKIP_TIPPING = "com.example.stripe_2.fragment.event.EventFragment.skip_tipping"
        private const val EXTENDED_AUTH = "com.example.stripe_2.fragment.event.EventFragment.extended_auth"
        private const val INCREMENTAL_AUTH = "com.example.stripe_2.fragment.event.EventFragment.incremental_auth"

        private lateinit var tripId: String
        private lateinit var readerId: String
        fun readReusableCard(): EventFragment {
            val fragment = EventFragment()
            val bundle = Bundle()
            bundle.putBoolean(READ_REUSABLE_CARD, true)
            bundle.putBoolean(REQUEST_PAYMENT, false)
            fragment.arguments = bundle
            return fragment
        }

        fun requestPayment(
            amount: Long,
            customerEmail: String,
            currentTripId: String,
            currentReaderId: String,
//            currency: String,
//            skipTipping: Boolean,
//            extendedAuth: Boolean,
//            incrementalAuth: Boolean
        ): EventFragment {
            val fragment = EventFragment()
            val bundle = Bundle()
            bundle.putLong(AMOUNT, amount)
            bundle.putString(EMAIL, customerEmail)
//            bundle.putString(CURRENCY, currency)
            bundle.putBoolean(REQUEST_PAYMENT, true)
            bundle.putBoolean(READ_REUSABLE_CARD, false)
//            bundle.putBoolean(SKIP_TIPPING, skipTipping)
//            bundle.putBoolean(EXTENDED_AUTH, extendedAuth)
//            bundle.putBoolean(INCREMENTAL_AUTH, incrementalAuth)
            tripId = currentTripId
            readerId = currentReaderId
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var adapter: EventAdapter
    private lateinit var eventRecyclerView: RecyclerView
    private lateinit var activityRef: WeakReference<FragmentActivity?>

    private lateinit var binding: FragmentEventBinding
    private lateinit var viewModel: EventViewModel

    private var paymentIntent: PaymentIntent? = null

    private val processPaymentCallback by lazy {
        object : PaymentIntentCallback {
            override fun onSuccess(paymentIntent: PaymentIntent) {
                addEvent("Processed payment", "terminal.processPayment")
                println("----------------------------------------PAYMENT PROCESSED CREATED")
                ApiClient.capturePaymentIntent(paymentIntent.id!!, tripId, readerId)
                addEvent("Captured PaymentIntent", "backend.capturePaymentIntent")
                completeFlow()
            }

            override fun onFailure(e: TerminalException) {
                this@EventFragment.onFailure(e)
            }
        }
    }

    private val cancelPaymentIntentCallback by lazy {
        object : PaymentIntentCallback {
            override fun onSuccess(paymentIntent: PaymentIntent) {
                addEvent("Canceled PaymentIntent", "terminal.cancelPaymentIntent")
                activityRef.get()?.let {
                    if (it is NavigationListener) {
                        it.runOnUiThread {
                            it.onCancelCollectPaymentMethod()
                        }
                    }
                }
            }

            override fun onFailure(e: TerminalException) {
                this@EventFragment.onFailure(e)
            }
        }
    }

    private val collectPaymentMethodCallback by lazy {
        object : PaymentIntentCallback {
            override fun onSuccess(paymentIntent: PaymentIntent) {
                addEvent("Collected PaymentMethod", "terminal.collectPaymentMethod")
                println("----------------------------------------PAYMENT INTENT METGOD COLLECTED")
                Terminal.getInstance().processPayment(paymentIntent, processPaymentCallback)
                viewModel.collectTask = null
            }

            override fun onFailure(e: TerminalException) {
                this@EventFragment.onFailure(e)
            }
        }
    }

    private val createPaymentIntentCallback by lazy {
        object : PaymentIntentCallback {
            override fun onSuccess(paymentIntent: PaymentIntent) {
                val skipTipping = arguments?.getBoolean(SKIP_TIPPING) ?: false
                val collectConfig = CollectConfiguration.Builder()
                    .skipTipping(skipTipping)
                    .build()
                this@EventFragment.paymentIntent = paymentIntent
                println("----------------------------------------PAYMENT INTENT CREATED")
                addEvent("Created PaymentIntent", "terminal.createPaymentIntent")
                viewModel.collectTask = Terminal.getInstance().collectPaymentMethod(
                    paymentIntent, collectPaymentMethodCallback, collectConfig
                )
            }

            override fun onFailure(e: TerminalException) {
                this@EventFragment.onFailure(e)
            }
        }
    }

    private val reusablePaymentMethodCallback by lazy {
        object : PaymentMethodCallback {
            override fun onSuccess(paymentMethod: PaymentMethod) {
                addEvent("Created PaymentMethod: ${paymentMethod.id}", "terminal.readReusableCard")
                completeFlow()
            }

            override fun onFailure(e: TerminalException) {
                this@EventFragment.onFailure(e)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityRef = WeakReference(activity)
        viewModel = ViewModelProvider(this)[EventViewModel::class.java]
        adapter = EventAdapter(viewModel)

        if (savedInstanceState == null) {
            arguments?.let {
                if (it.getBoolean(REQUEST_PAYMENT)) {
                    val extendedAuth = it.getBoolean(EXTENDED_AUTH)
                    val incrementalAuth = it.getBoolean(INCREMENTAL_AUTH)
                    val cardPresentParametersBuilder = CardPresentParameters.Builder()
                    if (extendedAuth) {
                        cardPresentParametersBuilder.setRequestExtendedAuthorization(true)
                    }
                    if (incrementalAuth) {
                        cardPresentParametersBuilder.setRequestIncrementalAuthorizationSupport(true)
                    }

                    val paymentMethodOptionsParameters = PaymentMethodOptionsParameters.Builder()
                        .setCardPresentParameters(cardPresentParametersBuilder.build())
                        .build()

                    val params = PaymentIntentParameters.Builder()
                        .setAmount(it.getLong(AMOUNT))
                        .setCurrency("eur")
                        .setMetadata(mapOf(
                            "tripId" to tripId,
                            "readerId" to readerId
                        ))
                        .setPaymentMethodOptionsParameters(paymentMethodOptionsParameters)
                        .setReceiptEmail(it.getString(EMAIL)!!)
                        .build()
                    println("----------------------------------------CREATING PAYMENT INTENT")
                    Terminal.getInstance()
                        .createPaymentIntent(params, createPaymentIntentCallback)
                } else if (it.getBoolean(READ_REUSABLE_CARD)) {
                    viewModel.collectTask = Terminal.getInstance().readReusableCard(
                        ReadReusableCardParameters.NULL, reusablePaymentMethodCallback
                    )
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        eventRecyclerView = binding.eventRecyclerView
        eventRecyclerView.layoutManager = LinearLayoutManager(activity)
        eventRecyclerView.adapter = adapter

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.cancelButton.setOnClickListener {
            activityRef.get()?.let {
                if (it is NavigationListener) {
                    it.runOnUiThread {
                        it.onRequestExitWorkflow()
                    }
                }
            }
            viewModel.collectTask?.cancel(object : Callback {
                override fun onSuccess() {
                    viewModel.collectTask = null
                    paymentIntent?.let {
                        if (TerminalFragment.getCurrentDiscoveryMethod(activityRef.get()) == DiscoveryMethod.INTERNET) {
                            ApiClient.cancelPaymentIntent(
                                paymentIntent!!.id!!,
                                object : retrofit2.Callback<Void> {
                                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                        if (response.isSuccessful) {
                                            addEvent("Canceled PaymentIntent", "backend.cancelPaymentIntent")
                                            activityRef.get()?.let { activity ->
                                                if (activity is NavigationListener) {
                                                    activity.runOnUiThread {
                                                        activity.onCancelCollectPaymentMethod()
                                                    }
                                                }
                                            }
                                        } else {
                                            addEvent("Cancel PaymentIntent failed", "backend.cancelPaymentIntent")
                                            completeFlow()
                                        }
                                    }

                                    override fun onFailure(call: Call<Void>, t: Throwable) {
                                        Toast.makeText(activity, t.message, Toast.LENGTH_LONG).show()
                                        completeFlow()
                                    }
                                }
                            )
                        } else {
                            Terminal.getInstance().cancelPaymentIntent(it, cancelPaymentIntentCallback)
                        }
                    }
                }

                override fun onFailure(e: TerminalException) {
                    viewModel.collectTask = null
                    this@EventFragment.onFailure(e)
                }
            })
        }

        binding.doneButton.setOnClickListener {
            activityRef.get()?.let {
                if (it is NavigationListener) {
                    it.runOnUiThread {
                        it.onRequestExitWorkflow()
                    }
                }
            }
        }
    }

    override fun onRequestReaderDisplayMessage(message: ReaderDisplayMessage) {
        addEvent(message.toString(), "listener.onRequestReaderDisplayMessage")
    }

    override fun onRequestReaderInput(options: ReaderInputOptions) {
        addEvent(options.toString(), "listener.onRequestReaderInput")
    }

    fun completeFlow() {
        activityRef.get()?.let {
            it.runOnUiThread {
                viewModel.isComplete.value = true
            }
        }
    }

    fun addEvent(message: String, method: String) {
        activityRef.get()?.let { activity ->
            activity.runOnUiThread {
                viewModel.addEvent(Event(message, method))
            }
        }
    }

    private fun onFailure(e: TerminalException) {
        addEvent(e.errorMessage, e.errorCode.toString())
        completeFlow()
    }
}