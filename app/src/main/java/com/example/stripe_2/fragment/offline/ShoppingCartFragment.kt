package com.example.stripe_2.fragment.offline

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.stripe_2.NavigationListener
import com.example.stripe_2.R
import com.example.stripe_2.databinding.FragmentEventBinding
import com.example.stripe_2.fragment.TerminalFragment
import com.example.stripe_2.fragment.event.EventAdapter
import com.example.stripe_2.fragment.event.EventFragment
import com.example.stripe_2.model.Event
import com.example.stripe_2.network.ApiClient
import com.example.stripe_2.viewmodel.EventViewModel
import com.stripe.stripeterminal.Terminal
import com.stripe.stripeterminal.external.OfflineMode
import com.stripe.stripeterminal.external.callable.Callback
import com.stripe.stripeterminal.external.callable.PaymentIntentCallback
import com.stripe.stripeterminal.external.models.*
import retrofit2.Call
import retrofit2.Response
import java.lang.ref.WeakReference


class ShoppingCartFragment : Fragment() {
    companion object {
        const val TAG = "com.example.stripe_2.fragment.offline.ShoppingCartFragment"

        private const val EMAIL = "com.example.stripe_2.fragment.offline.ShoppingCartFragment.email"
        private const val AMOUNT = "com.example.stripe_2.fragment.offline.ShoppingCartFragment.amount"
        private const val CURRENCY = "com.example.stripe_2.fragment.offline.ShoppingCartFragment.currency"
        private const val REQUEST_PAYMENT = "com.example.stripe_2.fragment.offline.ShoppingCartFragment.request_payment"
        private const val READ_REUSABLE_CARD = "com.example.stripe_2.fragment.offline.ShoppingCartFragment.read_reusable_card"
        private const val SKIP_TIPPING = "com.example.stripe_2.fragment.offline.ShoppingCartFragment.skip_tipping"
        private const val EXTENDED_AUTH = "com.example.stripe_2.fragment.offline.ShoppingCartFragment.extended_auth"
        private const val INCREMENTAL_AUTH = "com.example.stripe_2.fragment.offline.ShoppingCartFragment.incremental_auth"
        private lateinit var tripId: String
        private lateinit var readerId: String
        fun requestOfflinePayment(
        amount: Long,
        customerEmail: String,
        currentTripId: String,
        currentReaderId: String
//        skipTipping: Boolean,
//        extendedAuth: Boolean,
//        incrementalAuth: Boolean
        ): ShoppingCartFragment {
            val fragment = ShoppingCartFragment()
            val bundle = Bundle()
            bundle.putLong(AMOUNT, amount)
            bundle.putString(EMAIL, customerEmail)
//            bundle.putString(CURRENCY, currency)
            bundle.putBoolean(REQUEST_PAYMENT, true)
            bundle.putBoolean(READ_REUSABLE_CARD, false)
//            bundle.putBoolean(SKIP_TIPPING, skipTipping)
//            bundle.putBoolean(EXTENDED_AUTH, extendedAuth)
//            bundle.putBoolean(INCREMENTAL_AUTH, incrementalAuth)
            tripId = currentTripId
            readerId = currentReaderId
            fragment.arguments = bundle
            return fragment
        }
    }
    private lateinit var adapter: EventAdapter
    private lateinit var eventRecyclerView: RecyclerView
    private lateinit var activityRef: WeakReference<FragmentActivity?>

    private lateinit var binding: FragmentEventBinding
    private var paymentIntent: PaymentIntent? = null

    private lateinit var viewModel: EventViewModel

    private val createPaymentIntentCallback: PaymentIntentCallback by lazy {
        object : PaymentIntentCallback {
            override fun onSuccess(paymentIntent: PaymentIntent) {
                println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!createPaymentIntent succeeded")
                // If the `PaymentIntent` was created offline, it will have a non-null
                // offline id, and its `id` field will be null.
                addEvent("Created PaymentIntent", "terminal.createPaymentIntent")
                val collectConfig = CollectConfiguration.Builder().build()

                viewModel.collectTask = Terminal.getInstance().collectPaymentMethod(
                    paymentIntent, collectPaymentMethodCallback, collectConfig
                )
                if (paymentIntent.id != null) {
                    println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! created online")
                } else {
//                    val id = paymentIntent.offlineDetails?.id
                    println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!created offline with id: ${id ?: "Unexpected missing id"}")
                }
                // ... Collect a PaymentMethod
            }

            override fun onFailure(e: TerminalException) {
                println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!createPaymentIntent failed")
                println(e)
                println(e.errorMessage)
                println(e.errorCode)
                // Handle offline-specific errors in your application (for example,
                // `failIfOffline` was set to true and the SDK is offline).
            }
        }
    }

    private val collectPaymentMethodCallback by lazy {
        object : PaymentIntentCallback {
            override fun onSuccess(paymentIntent: PaymentIntent) {
                addEvent("Collected PaymentMethod", "terminal.collectPaymentMethod")

                Terminal.getInstance().processPayment(paymentIntent, processPaymentCallback)
                viewModel.collectTask = null

                println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!collectPaymentMethod succeeded")
                // ... Process the payment
            }

            override fun onFailure(e: TerminalException) {
                // Handle offline-specific errors in your application (for example,
                // unsupported payment method).
                println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!collectPaymentMethod failed:")
                activityRef.get()?.let { activity ->
                    if (activity is NavigationListener) {
                        activity.runOnUiThread {
                            activity.onCancelCollectPaymentMethod()
                        }
                    }
                }
            }
        }
    }

    private val processPaymentCallback by lazy {
        object : PaymentIntentCallback {
            override fun onSuccess(paymentIntent: PaymentIntent) {
                Terminal.getInstance().processPayment(paymentIntent, callback)
                addEvent("Processed payment", "terminal.processPayment")
                completeFlow()

                ApiClient.capturePaymentIntent(paymentIntent.id!!, tripId, readerId)
                addEvent("Captured PaymentIntent", "backend.capturePaymentIntent")
                println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!processPayment succeeded")
            }

            override fun onFailure(e: TerminalException) {
                // Handle offline-specific errors in your application (for example,
                // unsupported payment method).

                println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!processPayment failed:")
            }
        }
    }

    @OptIn(OfflineMode::class)
    private val callback by lazy {
        object : PaymentIntentCallback {
            override fun onSuccess(paymentIntent: PaymentIntent) {
                println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!processPayment succeeded")
                if (paymentIntent.status == PaymentIntentStatus.REQUIRES_CAPTURE) {
                    val offlineDetails = paymentIntent.offlineDetails
                    if (offlineDetails?.requiresUpload == true) {
//                     Offline payment, wait for `onPaymentIntentForwarded` (see snippet below)
                    } else {
//                     Online payment, can be captured now
                    }
                } else {
                    // handle other paymentIntent.status results here
                }
            }

            override fun onFailure(e: TerminalException) {
                // Handle offline-specific errors in your application (for example,
                // unsupported payment method).
                println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!processPayment failed:")
            }
        }
    }

    private val cancelPaymentIntentCallback by lazy {
        object : PaymentIntentCallback {
            override fun onSuccess(paymentIntent: PaymentIntent) {
                addEvent("Canceled PaymentIntent", "terminal.cancelPaymentIntent")
                activityRef.get()?.let {
                    if (it is NavigationListener) {
                        it.runOnUiThread {
                            it.onCancelCollectPaymentMethod()
                        }
                    }
                }
            }

            override fun onFailure(e: TerminalException) {

            }
        }
    }

    // Action for a "Checkout" button
    @OptIn(OfflineMode::class)
    private fun onCheckout(amount: Long, customerEmail: String) {
        // Build up parameters for creating a `PaymentIntent`
        val params = PaymentIntentParameters.Builder()
            .setAmount(amount)
            .setCurrency("eur")
            .setReceiptEmail(customerEmail)
            .build()
        // Your app might want to prevent offline payments for too large an amount.
        // Here, we block the payment if the amount is over 1000 eur.
        val failIfOffline = amount > 100000
        val createConfig = CreateConfiguration(failIfOffline)
        Terminal.getInstance().createPaymentIntent(params, createPaymentIntentCallback, createConfig)

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        activityRef = WeakReference(activity)
        viewModel = ViewModelProvider(this)[EventViewModel::class.java]
        adapter = EventAdapter(viewModel)
        println("-------------------------------------------------------------------------CREATING THE SHOPPING CART")
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            arguments?.let {
                this.onCheckout(amount = it.getLong(AMOUNT), customerEmail = it.getString(EMAIL)!!)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        eventRecyclerView = binding.eventRecyclerView
        eventRecyclerView.layoutManager = LinearLayoutManager(activity)
        eventRecyclerView.adapter = adapter

        return binding.root
    }

    @OptIn(OfflineMode::class)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.cancelButton.setOnClickListener {
            viewModel.collectTask?.cancel(object : Callback {
                override fun onFailure(e: TerminalException) {
                    viewModel.collectTask = null
                }

                override fun onSuccess() {
                    viewModel.collectTask = null
                    paymentIntent?.let {
                        Terminal.getInstance()
                            .cancelPaymentIntent(paymentIntent!!, cancelPaymentIntentCallback)
                    }
            }})
        }

        binding.doneButton.setOnClickListener {
            activityRef.get()?.let {
                if (it is NavigationListener) {
                    it.runOnUiThread {
                        it.onRequestExitWorkflow()
                    }
                }
            }
        }
    }

    fun completeFlow() {
        activityRef.get()?.let {
            it.runOnUiThread {
                viewModel.isComplete.value = true
            }
        }
    }

    fun addEvent(message: String, method: String) {
        activityRef.get()?.let { activity ->
            activity.runOnUiThread {
                viewModel.addEvent(Event(message, method))
            }
        }
    }

}