package com.example.stripe_2.fragment.offline

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

class NetworkStatusFragment(val context: Context) {
    private val networkStatus = TextView(context).apply {
        text = ""
        textSize = 12f
        gravity = Gravity.CENTER
        layoutParams = ViewGroup.LayoutParams(
            dpToPx(75),
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val horizontalPadding = dpToPx(8)
        val verticalPadding = dpToPx(2)
        setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding)
        setTextColor(Color.WHITE)
    }

    val view: View = networkStatus

    private fun View.dpToPx(dp: Int): Int {
        return (dp * resources.displayMetrics.density).toInt()
    }

    fun setText(text: CharSequence) {
        networkStatus.text = text
    }
}