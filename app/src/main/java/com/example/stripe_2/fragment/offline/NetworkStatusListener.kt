package com.example.stripe_2.fragment.offline

import com.stripe.stripeterminal.external.models.NetworkStatus

interface NetworkStatusListener {
    fun onNetworkStatusChanged(networkStatus: NetworkStatus)
}