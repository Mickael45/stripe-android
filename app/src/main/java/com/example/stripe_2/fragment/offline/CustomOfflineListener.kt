package com.example.stripe_2.fragment.offline

import com.example.stripe_2.MainActivity
import com.example.stripe_2.network.ApiClient
import com.stripe.stripeterminal.Terminal
import com.stripe.stripeterminal.external.OfflineMode
import com.stripe.stripeterminal.external.callable.OfflineListener
import com.stripe.stripeterminal.external.models.*
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking

@OptIn(OfflineMode::class)
class CustomOfflineListener(val listener: MainActivity) : OfflineListener {
    override fun onNetworkStatusChange(networkStatus: NetworkStatus) {
        if (listener != null) {
            listener.onNetworkStatusChanged(networkStatus)
        }
        // Check the value of `networkStatus` and update your UI accordingly.
        //
        // Note that you may also check the SDK's current network status using
        // `Terminal::networkStatus`.
    }

    override fun onPaymentIntentForwarded(paymentIntent: PaymentIntent, e: TerminalException?) {
        if (e != null) {
            // Handle the error appropriate for your application
        } else if (paymentIntent.status == PaymentIntentStatus.REQUIRES_CAPTURE) {
            listener.updateOfflinePendingPaymentIntentStatus("SYNCING")

           runBlocking {
                async {
                    listener.currentTripId?.let {
                        listener.currentReaderId?.let { it1 ->
                            ApiClient.capturePaymentIntent(paymentIntent.id!!,
                                it, it1
                            )
                        }
                    }
                }.await()

                if (Terminal.getInstance().offlinePaymentsCount == 1) {
                    listener.updateOfflinePendingPaymentIntentStatus("SYNCED")
                }
            }
        } else {
            // Handle the paymentIntent.status as appropriate.
        }
    }

    override fun onForwardingFailure(e: TerminalException) {
        if (listener != null) {
            listener.updateOfflinePendingPaymentIntentStatus("ERROR")
        }
        // A non-specific error occurred while forwarding a PaymentIntent.
        // Check the error message and your integration implementation to
        // troubleshoot.
    }
}