package com.example.stripe_2.fragment.offline

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

class OfflinePendingPaymentIntentStatusFragment(val context: Context) {
    private val pendingPaymentIntentText = TextView(context).apply {
        text = "Offline Payment Status"
        textSize = 18f
        gravity = Gravity.END
        val verticalPadding = dpToPx(2)
        setPadding(0, verticalPadding, 0, verticalPadding)
        setTextColor(Color.WHITE)
    }

    private val pendingPaymentIntentStatus = TextView(context).apply {
        text = ""
        textSize = 12f
        layoutParams = ViewGroup.LayoutParams(
            dpToPx(150),
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        gravity = Gravity.CENTER
        val horizontalPadding = dpToPx(8)
        val verticalPadding = dpToPx(2)
        setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding)
        setTextColor(Color.WHITE)
    }

    private  val linearVerticalLayout = LinearLayout(context).apply {
        orientation = LinearLayout.VERTICAL
        gravity = Gravity.END
        layoutParams = LinearLayout.LayoutParams(
            0,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            1f
        )

        addView(pendingPaymentIntentText)
        addView(pendingPaymentIntentStatus)
    }


    val view: View = linearVerticalLayout
    val pendingPaymentIntentStatusView: View = pendingPaymentIntentStatus

    private fun View.dpToPx(dp: Int): Int {
        return (dp * resources.displayMetrics.density).toInt()
    }

    fun setText(text: CharSequence) {
        pendingPaymentIntentStatus.text = text
    }

    fun setColor(color: Int) {
        pendingPaymentIntentStatus.setTextColor(color)
    }
}