package com.example.stripe_2.fragment.offline

import com.stripe.stripeterminal.external.models.NetworkStatus

interface OfflinePendingPaymentIntentStatusListener {
    fun updateOfflinePendingPaymentIntentStatus(globalPaymentIntentStatus: String)
}