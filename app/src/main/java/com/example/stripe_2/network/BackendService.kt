package com.example.stripe_2.network

import com.example.stripe_2.model.ConnectionToken
import com.example.stripe_2.model.LocationInfo
import com.example.stripe_2.model.TripInfo
import com.example.stripe_2.model.TripReportInfo
import com.example.stripe_2.model.TripReportInfoResponse
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * The `BackendService` interface handles the two simple calls we need to make to our backend.
 */
interface BackendService {

    /**
     * Get a connection token string from the backend
     */
    @POST("connection_token")
    fun getConnectionToken(): Call<ConnectionToken>

    /**
     * Capture a specific payment intent on our backend
     */
    @FormUrlEncoded
    @POST("capture_payment_intent")
    fun capturePaymentIntent(@Field("payment_intent_id") id: String, @Field("tripId") tripId: String, @Field("readerId") readerId: String): Call<Void>

    /**
     * Cancel a specific payment intent on our backend
     */
    @FormUrlEncoded
    @POST("cancel_payment_intent")
    fun cancelPaymentIntent(@Field("payment_intent_id") id: String): Call<Void>

    /**
     * Creates a new location
     */
    @Headers("Content-Type: application/json")
    @POST("create_location")
    fun createLocation(@Body locationInfo: LocationInfo): Call<LocationInfo>

    @GET("ipe/")
    fun getBoatsNextTripInfo(@Query("navi") navi: Int): Call<TripInfo>


    @Headers("Content-Type: application/json")
    @POST("create_trip_report")
    fun createTripReport(@Body tripInfo: TripReportInfo): Call<JsonObject>
}