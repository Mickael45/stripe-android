package com.example.stripe_2.network

import com.example.stripe_2.BuildConfig
import com.example.stripe_2.model.LocationInfo
import com.example.stripe_2.model.TripInfo
import com.example.stripe_2.model.TripReportInfo
import com.google.gson.JsonObject
import com.stripe.stripeterminal.external.models.ConnectionTokenException
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


/**
 * The `ApiClient` is a singleton object used to make calls to our backend and return their results
 */
object ApiClient {

    private val client = OkHttpClient.Builder()
        .build()
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.EXAMPLE_BACKEND_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    private val service: BackendService = retrofit.create(BackendService::class.java)

    private val retrofit2: Retrofit = Retrofit.Builder()
        .baseUrl("https://www.corsicaferries.com/")
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    private val service2: BackendService = retrofit2.create(BackendService::class.java)


    @Throws(ConnectionTokenException::class)
    internal fun createConnectionToken(): String {
        try {
            val result = service.getConnectionToken().execute()
            if (result.isSuccessful && result.body() != null) {
                return result.body()!!.secret
            } else {
                throw ConnectionTokenException("Creating connection token failed")
            }
        } catch (e: IOException) {
            throw ConnectionTokenException("Creating connection token failed", e)
        }
    }

    internal fun createLocation(
        displayName: String,
        city: String,
        country: String,
        line1: String,
        line2: String?,
        postalCode: String,
        state: String?,
    ) {
        val call: Call<LocationInfo> = service.createLocation(
            LocationInfo(
                displayName=displayName,
                line1=line1,
                line2=line2,
                postalCode=postalCode,
                city=city,
                state=state,
                country=country
            ))
        call.enqueue(object : Callback<LocationInfo?> {
            override fun onResponse(call: Call<LocationInfo?>, response: Response<LocationInfo?>) {
            }

            override fun onFailure(call: Call<LocationInfo?>, t: Throwable) {
            }
        })
    }

    internal fun capturePaymentIntent(id: String, tripId: String, readerId: String) {
        service.capturePaymentIntent(id, tripId, readerId).execute()
    }

    internal fun cancelPaymentIntent(
        id: String,
        callback: Callback<Void>
    ) {
        service.cancelPaymentIntent(id).enqueue(callback)
    }

    internal fun getBoatsNextTripInfo(
        connectLocation: String,
        callback: Callback<TripInfo>
    ) {
        service2.getBoatsNextTripInfo(connectLocation.split("|")[0].trim().toInt()).enqueue(callback)
    }

    internal fun createTripReport(
        locationId: String,
        tripId: String,
        readerId: String,
        requestGenerationCallback: Callback<JsonObject>
    ) {
        service.createTripReport(
            TripReportInfo(
                locationId,
            tripId,
            readerId
        )
        ).enqueue(requestGenerationCallback)
    }
}

