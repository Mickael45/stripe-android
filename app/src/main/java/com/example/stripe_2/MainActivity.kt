package com.example.stripe_2

import android.Manifest
import android.app.ProgressDialog.show
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.stripe_2.fragment.ConnectedReaderFragment
import com.example.stripe_2.fragment.offline.NetworkStatusFragment
import com.example.stripe_2.fragment.PaymentFragment
import com.example.stripe_2.fragment.TerminalFragment
import com.example.stripe_2.fragment.UpdateReaderFragment
import com.example.stripe_2.fragment.discovery.DiscoveryFragment
import com.example.stripe_2.fragment.event.EventFragment
import com.example.stripe_2.fragment.location.LocationCreateFragment
import com.example.stripe_2.fragment.location.LocationSelectionController
import com.example.stripe_2.fragment.location.LocationSelectionFragment
import com.example.stripe_2.fragment.offline.CustomOfflineListener
import com.example.stripe_2.fragment.offline.NetworkStatusListener
import com.example.stripe_2.fragment.offline.OfflinePendingPaymentIntentStatusFragment
import com.example.stripe_2.fragment.offline.OfflinePendingPaymentIntentStatusListener
import com.example.stripe_2.fragment.offline.ShoppingCartFragment
import com.example.stripe_2.model.TripInfo
import com.example.stripe_2.network.ApiClient
import com.example.stripe_2.network.TokenProvider
import com.google.gson.JsonObject
import com.stripe.stripeterminal.Terminal
import com.stripe.stripeterminal.external.OfflineMode
import com.stripe.stripeterminal.external.callable.BluetoothReaderListener
import com.stripe.stripeterminal.external.callable.Cancelable
import com.stripe.stripeterminal.external.callable.UsbReaderListener
import com.stripe.stripeterminal.external.models.ConnectionStatus
import com.stripe.stripeterminal.external.models.DiscoveryMethod
import com.stripe.stripeterminal.external.models.Location
import com.stripe.stripeterminal.external.models.NetworkStatus
import com.stripe.stripeterminal.external.models.ReaderDisplayMessage
import com.stripe.stripeterminal.external.models.ReaderInputOptions
import com.stripe.stripeterminal.external.models.ReaderSoftwareUpdate
import com.stripe.stripeterminal.external.models.TerminalException
import com.stripe.stripeterminal.log.LogLevel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity :
    AppCompatActivity(),
    NetworkStatusListener,
    OfflinePendingPaymentIntentStatusListener,
    NavigationListener,
    BluetoothReaderListener,
    UsbReaderListener,
    LocationSelectionController {

    // Register the permissions callback to handles the response to the system permissions dialog.
    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions(),
        ::onPermissionResult
    )

    private var customOfflineListener: CustomOfflineListener? = null
    var currentTripId: String? = null
    var currentReaderId: String? = null
    var currentLocationId: String? = null
    lateinit var networkStatusFragment: NetworkStatusFragment
    lateinit var offlinePendingPaynemtRequesStatusFragment: OfflinePendingPaymentIntentStatusFragment
    /**
     * Upon starting, we should verify we have the permissions we need, then start the app
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        requestPermissionsIfNecessary()

        if (
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.BLUETOOTH_CONNECT
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            BluetoothAdapter.getDefaultAdapter()?.let { adapter ->
                if (!adapter.isEnabled) {
                    adapter.enable()
                }
            }
        } else {
            Log.w(MainActivity::class.java.simpleName, "Failed to acquire Bluetooth permission")
        }

        networkStatusFragment = NetworkStatusFragment(this)
        offlinePendingPaynemtRequesStatusFragment = OfflinePendingPaymentIntentStatusFragment(this)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM

        supportActionBar?.setDisplayShowCustomEnabled(true)

        val appName = TextView(this).apply {
            text = "Stripe Payment"
            textSize = 24f
            setTextColor(Color.WHITE)
        }

        val linearVerticalLayout = LinearLayout(this).apply {
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.CENTER_VERTICAL
            layoutParams = LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1f
            )
            addView(appName)
            addView(networkStatusFragment.view)
        }

        val linearHorizontalLayout = LinearLayout(this).apply {
            orientation = LinearLayout.HORIZONTAL
            gravity = Gravity.CENTER_VERTICAL
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            addView(linearVerticalLayout)
            addView(offlinePendingPaynemtRequesStatusFragment.view)
        }

        supportActionBar?.customView = linearHorizontalLayout

    }

    @OptIn(OfflineMode::class)
    override fun onStart() {
        super.onStart()
        setCustomOfflineListener()
        if (Terminal.isInitialized()) {
            onNetworkStatusChanged(Terminal.getInstance().networkStatus)
        }
    }

    private fun setCustomOfflineListener(): CustomOfflineListener? {
        customOfflineListener = CustomOfflineListener(this)
        return customOfflineListener
    }

    private fun getCustomOfflineListener(): CustomOfflineListener? {
        if (customOfflineListener == null) {
            return setCustomOfflineListener()
        }
        return customOfflineListener
    }

    override fun updateOfflinePendingPaymentIntentStatus(globalPaymentIntentStatus: String) {
        val drawable = resources.getDrawable(R.drawable.rounded_corners, theme) as GradientDrawable

        if (globalPaymentIntentStatus == "WAITING FOR CONNECTION") {
            offlinePendingPaynemtRequesStatusFragment.setText("WAITING FOR CONNECTION")
            offlinePendingPaynemtRequesStatusFragment.setColor(Color.WHITE)
            drawable.setColor(Color.rgb(255, 165, 0))
        } else if (globalPaymentIntentStatus == "SYNCING") {
            offlinePendingPaynemtRequesStatusFragment.setText("SYNCING")
            offlinePendingPaynemtRequesStatusFragment.setColor(Color.BLACK)
            drawable.setColor(Color.YELLOW)
        } else if (globalPaymentIntentStatus == "ERROR") {
            offlinePendingPaynemtRequesStatusFragment.setText("ERROR")
            offlinePendingPaynemtRequesStatusFragment.setColor(Color.WHITE)
            drawable.setColor(Color.RED)
        } else if (globalPaymentIntentStatus == "SYNCED") {
            offlinePendingPaynemtRequesStatusFragment.setText("SYNCED")
            offlinePendingPaynemtRequesStatusFragment.setColor(Color.WHITE)
            drawable.setColor(Color.GREEN)
        } else if (globalPaymentIntentStatus == "NONE") {
            offlinePendingPaynemtRequesStatusFragment.setText("")
            drawable.setColor(Color.TRANSPARENT)
        }
        offlinePendingPaynemtRequesStatusFragment.pendingPaymentIntentStatusView.background = drawable

    }
    @OptIn(OfflineMode::class)
    override fun onNetworkStatusChanged(networkStatus: NetworkStatus) {
        val drawable = resources.getDrawable(R.drawable.rounded_corners, theme) as GradientDrawable

        if (networkStatus == NetworkStatus.OFFLINE) {
            networkStatusFragment.setText("OFFLINE")
            drawable.setColor(Color.RED)
            updateOfflinePendingPaymentIntentStatus("WAITING FOR CONNECTION")
        } else {
            networkStatusFragment.setText("ONLINE")
            drawable.setColor(Color.GREEN)
            updateOfflinePendingPaymentIntentStatus("NONE")
        }
        networkStatusFragment.view.background = drawable
    }


    @OptIn(OfflineMode::class)
    override fun onResume() {
        super.onResume()
        requestPermissionsIfNecessary()
    }

    private fun requestPermissionsIfNecessary() {
        if (Build.VERSION.SDK_INT >= 31) {
            requestPermissionsIfNecessarySdk31()
        } else {
            requestPermissionsIfNecessarySdkBelow31()
        }
    }

    private fun isGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissionsIfNecessarySdkBelow31() {
        // Check for location permissions
        if (!isGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            // If we don't have them yet, request them before doing anything else
            requestPermissionLauncher.launch(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION))
        } else if (!Terminal.isInitialized() && verifyGpsEnabled()) {
            initialize()
        }
    }

    @RequiresApi(Build.VERSION_CODES.S)
    private fun requestPermissionsIfNecessarySdk31() {
        // Check for location and bluetooth permissions
        val deniedPermissions = mutableListOf<String>().apply {
            if (!isGranted(Manifest.permission.ACCESS_FINE_LOCATION)) add(Manifest.permission.ACCESS_FINE_LOCATION)
            if (!isGranted(Manifest.permission.BLUETOOTH_CONNECT)) add(Manifest.permission.BLUETOOTH_CONNECT)
            if (!isGranted(Manifest.permission.BLUETOOTH_SCAN)) add(Manifest.permission.BLUETOOTH_SCAN)
        }.toTypedArray()

        if (deniedPermissions.isNotEmpty()) {
            // If we don't have them yet, request them before doing anything else
            requestPermissionLauncher.launch(deniedPermissions)
        } else if (!Terminal.isInitialized() && verifyGpsEnabled()) {
            initialize()
        }
    }

    /**
     * Receive the result of our permissions check, and initialize if we can
     */
    private fun onPermissionResult(result: Map<String, Boolean>) {
        val deniedPermissions: List<String> = result
            .filter { !it.value }
            .map { it.key }

        // If we receive a response to our permission check, initialize
        if (deniedPermissions.isEmpty() && !Terminal.isInitialized() && verifyGpsEnabled()) {
            initialize()
        }
    }

    // Navigation callbacks

    /**
     * Callback function called when discovery has been canceled by the [DiscoveryFragment]
     */
    override fun onCancelDiscovery() {
        navigateTo(TerminalFragment.TAG, TerminalFragment())
    }

    override fun onCancelLocation() {
        navigateTo(DiscoveryFragment.TAG, DiscoveryFragment())
    }

    override fun onCancelLocationCreation() {
        supportFragmentManager.popBackStackImmediate()
    }

    override fun onRequestChangeLocation() {
        navigateTo(
            LocationSelectionFragment.TAG,
            LocationSelectionFragment.newInstance(),
            replace = false,
            addToBackStack = true,
        )
    }

    override fun onRequestCreateLocation() {
        navigateTo(
            LocationCreateFragment.TAG,
            LocationCreateFragment.newInstance(),
            replace = false,
            addToBackStack = true,
        )
    }

    override fun onLocationCreated() {
        supportFragmentManager.popBackStackImmediate()
        (supportFragmentManager.fragments.last() as? LocationSelectionFragment)?.reload()
    }

    /**
     * Callback function called once discovery has been selected by the [TerminalFragment]
     */
    override fun onRequestDiscovery(isSimulated: Boolean, discoveryMethod: DiscoveryMethod) {
        navigateTo(DiscoveryFragment.TAG, DiscoveryFragment.newInstance(isSimulated, discoveryMethod))
    }

    private val requestGenerationCallback by lazy {
        object : Callback<JsonObject> {
            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                if (response.isSuccessful) {
                    val body = response.body().toString()
                    val message = JSONObject(body).getString("message")

                    Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT)
                        .show()
                } else {
                    val errorBody = response.errorBody()?.string()
                    val message = JSONObject(errorBody).getString("message")

                    Toast.makeText(this@MainActivity, "Error: ${message}", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Toast.makeText(this@MainActivity, "API call failed: ${t?.message}", Toast.LENGTH_SHORT).show()
            }

        }
    }

    override fun onRequestReportGeneration() {
        println("-------------------------------------")
        println(currentTripId)
        println(currentLocationId)
        println(currentReaderId)
        ApiClient
            .createTripReport(currentLocationId!!, currentTripId!!, currentReaderId!!, requestGenerationCallback)
    }


    /**
     * Callback function called to exit the payment workflow
     */
    override fun onRequestExitWorkflow() {
        if (Terminal.getInstance().connectionStatus == ConnectionStatus.CONNECTED) {
            navigateTo(ConnectedReaderFragment.TAG, ConnectedReaderFragment())
        } else {
            navigateTo(TerminalFragment.TAG, TerminalFragment())
        }
    }

    /**
     * Callback function called to start a payment by the [PaymentFragment]
     */
    override fun onRequestPayment(
        amount: Long,
        currency: String,
//        skipTipping: Boolean,
//        extendedAuth: Boolean,
//        incrementalAuth: Boolean
    ) {
        navigateTo(
            EventFragment.TAG, EventFragment.requestPayment(
                amount,
                currency,
                currentTripId!!,
                currentReaderId!!
//                skipTipping,
//                extendedAuth,
//                incrementalAuth
            )
        )
    }


    /**
     * Callback function called to start a checkout by the [PaymentFragment]
     */
    override fun onRequestOfflinePayment(
        amount: Long,
        currency: String,
//        skipTipping: Boolean,
//        extendedAuth: Boolean,
//        incrementalAuth: Boolean
    ) {
        navigateTo(
            ShoppingCartFragment.TAG, ShoppingCartFragment.requestOfflinePayment(
                amount,
                currency,
                currentTripId!!,
                currentReaderId!!
//                skipTipping,
//                extendedAuth,
//                incrementalAuth
            )
        )
    }
    /**
     * Callback function called once the payment workflow has been selected by the
     * [ConnectedReaderFragment]
     */
    override fun onSelectPaymentWorkflow() {
        navigateTo(PaymentFragment.TAG, PaymentFragment())
    }

    /**
     * Callback function called once the read card workflow has been selected by the
     * [ConnectedReaderFragment]
     */
    override fun onSelectReadReusableCardWorkflow() {
        navigateTo(EventFragment.TAG, EventFragment.readReusableCard())
    }

    /**
     * Callback function called once the update reader workflow has been selected by the
     * [ConnectedReaderFragment]
     */
    override fun onSelectUpdateWorkflow() {
        navigateTo(UpdateReaderFragment.TAG, UpdateReaderFragment())
    }

    // Terminal event callbacks

    /**
     * Callback function called when collect payment method has been canceled
     */
    override fun onCancelCollectPaymentMethod() {
        navigateTo(ConnectedReaderFragment.TAG, ConnectedReaderFragment())
    }


    private val onGetBoatsNextTripInfoResult by lazy {
        object : Callback<TripInfo> {
        override fun onResponse(call: Call<TripInfo?>, response: Response<TripInfo?>) {
            val data = response.body()
            if (data?.message == "result found") {
                currentTripId = data?.cod_lign + data?.date_dep?.replace("/", "") + data?.hor_dep?.replace(":", "")
                navigateTo(ConnectedReaderFragment.TAG, ConnectedReaderFragment())
            }
        }override fun onFailure(call: Call<TripInfo?>, t: Throwable) {
        }
    }}
    /**
     * Callback function called on completion of [Terminal.connectBluetoothReader]
     */
    override fun onConnectReader(connectLocationId: String, connectLocationName: String, readerId: String) {
        currentReaderId = readerId
        currentLocationId = connectLocationId
        ApiClient.getBoatsNextTripInfo(connectLocationName, onGetBoatsNextTripInfoResult)
    }

    override fun onDisconnectReader() {
        navigateTo(TerminalFragment.TAG, TerminalFragment())
    }

    override fun onStartInstallingUpdate(update: ReaderSoftwareUpdate, cancelable: Cancelable?) {
        runOnUiThread {
            // Delegate out to the current fragment, if it acts as a BluetoothReaderListener
            supportFragmentManager.fragments.last()?.let {
                if (it is BluetoothReaderListener) {
                    it.onStartInstallingUpdate(update, cancelable)
                }
            }
        }
    }

    override fun onReportReaderSoftwareUpdateProgress(progress: Float) {
        runOnUiThread {
            // Delegate out to the current fragment, if it acts as a BluetoothReaderListener
            supportFragmentManager.fragments.last()?.let {
                if (it is BluetoothReaderListener) {
                    it.onReportReaderSoftwareUpdateProgress(progress)
                }
            }
        }
    }

    override fun onFinishInstallingUpdate(update: ReaderSoftwareUpdate?, e: TerminalException?) {
        runOnUiThread {
            // Delegate out to the current fragment, if it acts as a BluetoothReaderListener
            supportFragmentManager.fragments.last()?.let {
                if (it is BluetoothReaderListener) {
                    it.onFinishInstallingUpdate(update, e)
                }
            }
        }
    }

    override fun onRequestReaderInput(options: ReaderInputOptions) {
        runOnUiThread {
            // Delegate out to the current fragment, if it acts as a BluetoothReaderListener
            supportFragmentManager.fragments.last()?.let {
                if (it is BluetoothReaderListener) {
                    it.onRequestReaderInput(options)
                }
            }
        }
    }

    override fun onRequestReaderDisplayMessage(message: ReaderDisplayMessage) {
        runOnUiThread {
            // Delegate out to the current fragment, if it acts as a BluetoothReaderListener
            supportFragmentManager.fragments.last()?.let {
                if (it is BluetoothReaderListener) {
                    it.onRequestReaderDisplayMessage(message)
                }
            }
        }
    }

    override fun onLocationSelected(location: Location) {
        supportFragmentManager.popBackStackImmediate()
        (supportFragmentManager.fragments.last() as? LocationSelectionController)?.onLocationSelected(location)
    }

    override fun onLocationCleared() {
        supportFragmentManager.popBackStackImmediate()
        currentTripId = null
        (supportFragmentManager.fragments.last() as? LocationSelectionController)?.onLocationCleared()
    }

    /**
     * Initialize the [Terminal] and go to the [TerminalFragment]
     */
    @OptIn(OfflineMode::class)
    private fun initialize() {
        // Initialize the Terminal as soon as possible

        try {
            Terminal.initTerminal(
                context = applicationContext,
                logLevel = LogLevel.VERBOSE,
                tokenProvider = TokenProvider(),
                listener = TerminalEventListener(),
                offlineListener = getCustomOfflineListener()
            )
        } catch (e: TerminalException) {
            throw RuntimeException(
                "Location services are required in order to initialize " +
                        "the Terminal.",
                e
            )
        }

        navigateTo(TerminalFragment.TAG, TerminalFragment())
    }

    /**
     * Navigate to the given fragment.
     *
     * @param fragment Fragment to navigate to.
     */
    private fun navigateTo(
        tag: String,
        fragment: Fragment,
        replace: Boolean = true,
        addToBackStack: Boolean = false,
    ) {
        val frag = supportFragmentManager.findFragmentByTag(tag) ?: fragment
        supportFragmentManager
            .beginTransaction()
            .apply {
                if (replace) {
                    replace(R.id.container, frag, tag)
                } else {
                    add(R.id.container, frag, tag)
                }

                if (addToBackStack) {
                    addToBackStack(tag)
                }
            }
            .commitAllowingStateLoss()
    }

    private fun verifyGpsEnabled(): Boolean {
        val locationManager: LocationManager? =
            applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        var gpsEnabled = false

        try {
            gpsEnabled = locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER) ?: false
        } catch (_: Exception) {
        }

        if (!gpsEnabled) {
            // notify user
            AlertDialog.Builder(
                ContextThemeWrapper(
                    this,
                    com.google.android.material.R.style.Theme_MaterialComponents_DayNight_DarkActionBar
                )
            )
                .setMessage("Please enable location services")
                .setCancelable(false)
                .setPositiveButton("Open location settings") { _, _ ->
                    this.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
                .create()
                .show()
        }

        return gpsEnabled
    }
}